package com.example.kiptum.application01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText text1, text2;
    private Button Convert;
    private Button Exit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        //getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_main);

        text1=(EditText) findViewById(R.id.millimeters);
        text2=(EditText) findViewById(R.id.inches);
        Convert=(Button) findViewById(R.id.convert);
        Exit=(Button) findViewById(R.id.exit);
        Convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String milli=text1.getText().toString().trim();
                if (TextUtils.isEmpty(milli)){
                    Toast.makeText(MainActivity.this, "The millimeter field cannot be empty!", Toast.LENGTH_SHORT).show();
                }
                else{
                     int millimeters=Integer.parseInt(milli);
                     double inches=millimeters/25.4;
                     text2.setText(String.valueOf(inches));
                }
            }
        });
        Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


}